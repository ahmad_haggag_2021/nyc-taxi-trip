
CREATE EXTERNAL SCHEMA nyc_taxi_trip 
FROM DATA CATALOG database 'nyc-taxi-capstone-ah' region 'us-east-1' 
iam_role 'arn:aws:iam::584910123950:role/NYC-Taxi-Trip-RedShift-AH';


select * from svv_external_schemas


SELECT  MONTH,
         trip_type,
         avg(trip_distance) AS avgDistance,
         avg(total_amount/trip_distance) AS avgCostPerMile,
         avg(total_amount) AS avgCost
FROM nyc_taxi_trip.partition_trip_data
WHERE year = 2009
        AND (trip_type = 'yellow'
        OR trip_type = 'green')
        AND trip_distance > 0
        AND total_amount > 0
GROUP BY   MONTH, trip_type
ORDER BY  MONTH


SELECT   extract(month from pickup_datetime) ,
         trip_type,
         avg(trip_distance) AS avgDistance,
         avg(total_amount/trip_distance) AS avgCostPerMile,
         avg(total_amount) AS avgCost
FROM nyc_taxi_trip.parquet_nyc_taxi_trip
WHERE  extract(year from pickup_datetime)  = 2009
        AND (trip_type = 'yellow'
        OR trip_type = 'green')
        AND trip_distance > 0
        AND total_amount > 0
GROUP BY    extract(month from pickup_datetime) , trip_type
ORDER BY  extract(month from pickup_datetime) 