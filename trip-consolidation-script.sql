-- trip_data.green_taxi definition
CREATE TABLE nyc_taxi_trip (
  `vendor_id` int DEFAULT NULL,
  `pickup_datetime` datetime DEFAULT NULL,
  `dropoff_datetime` datetime DEFAULT NULL,
  `store_and_fwd_flag` varchar(20) DEFAULT NULL,
  `rate_code_id` int DEFAULT NULL,
  `pu_location_id` int DEFAULT NULL,
  `do_location_id` int DEFAULT NULL,
  `passenger_count` int DEFAULT NULL,
  `trip_distance` decimal(5,2) DEFAULT NULL,
  `fare_amount` decimal(5,2) DEFAULT NULL,
  `extra` decimal(5,2) DEFAULT NULL,
  `mta_tax` decimal(5,2) DEFAULT NULL,
  `tip_amount` decimal(5,2) DEFAULT NULL,
  `tolls_amount` decimal(5,2) NOT NULL,
  `ehail_fee` decimal(5,2) DEFAULT NULL,
  `improvement_surcharge` decimal(5,2) DEFAULT NULL,
  `total_amount` decimal(5,2) DEFAULT NULL,
  `payment_type` int DEFAULT NULL,
  `trip_type` varchar(20) DEFAULT NULL,
  `congestion_surcharge` decimal(5,2) DEFAULT NULL
) ;


INSERT INTO nyc_taxi_trip
(vendor_id, pickup_datetime, dropoff_datetime, store_and_fwd_flag, rate_code_id, pu_location_id, do_location_id, passenger_count, trip_distance, fare_amount, extra, mta_tax, tip_amount, tolls_amount, ehail_fee, improvement_surcharge, total_amount, payment_type, trip_type, congestion_surcharge)
SELECT VendorID, lpep_pickup_datetime, lpep_dropoff_datetime, store_and_fwd_flag, RatecodeID, PULocationID, DOLocationID, passenger_count, trip_distance, fare_amount, extra, mta_tax, tip_amount, tolls_amount, ehail_fee, improvement_surcharge, total_amount, payment_type, 'green' , congestion_surcharge
FROM green_taxi;


INSERT INTO nyc_taxi_trip
(vendor_id, pickup_datetime, dropoff_datetime, store_and_fwd_flag, rate_code_id, pu_location_id, do_location_id, passenger_count, trip_distance, fare_amount, extra, mta_tax, tip_amount, tolls_amount, ehail_fee, improvement_surcharge, total_amount, payment_type, trip_type, congestion_surcharge)
SELECT VendorID, tpep_pickup_datetime, tpep_dropoff_datetime , store_and_fwd_flag , RatecodeID , PULocationID , DOLocationID , passenger_count, trip_distance, fare_amount , extra , mta_tax , tip_amount , tolls_amount , null , improvement_surcharge, total_amount, payment_type , 'yellow' , congestion_surcharge
FROM yellow_taxi;

SELECT sum(trip_count) , 'split_taxi_trip'
FROM (
SELECT 'green' ,  count(*) trip_count FROM green_taxi
UNION
SELECT 'yellow' , count(*) trip_count FROM yellow_taxi
) taxi_trip_count 
UNION 
SELECT count(*) , 'merge_taxi_trip' FROM nyc_taxi_trip 



select  date(pickup_datetime), YEAR(pickup_datetime) , MONTH(pickup_datetime) , DAYOFMONTH(pickup_datetime)  from nyc_taxi_trip ntt 
select  *  from nyc_taxi_trip ntt where DAYOFMONTH(pickup_datetime) is null

select distinct YEAR(pickup_datetime)   from nyc_taxi_trip ntt 


